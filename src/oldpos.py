'''
Created on 22.04.2019

@author: nils
'''

import re, csv, os, sys
from tabulate import tabulate
from subprocess import PIPE, Popen
from sklearn.linear_model import LogisticRegression

#-------------------------------------------------------------------------------------
DEVICES,LOCATIONS = [''],['...']

def dev(mac):
    if not mac in DEVICES:
        DEVICES.append(mac)
    return DEVICES.index(mac)
def loc(pos):
    if not pos in LOCATIONS:
        LOCATIONS.append(pos)
    return LOCATIONS.index(pos)

#-------------------------------------------------------------------------------------

def getX(xy):
    return [[el[3]] + el[5:] for el in xy]
def setY(el,Y):
    el[1], el[2] = Y, loc(Y)+100
    return el

#-------------------------------------------------------------------------------------

def _testdata():
    locs, data, td = ['wohnzimmer', 'gang','kueche'],[],[]
    data.append("Cell 01 - Address: abc\n Quality=12/70  Signal level=-24 dBm\n ESSID:\"Yoink\"\n   Cell 02 - Address: efg\n Quality=63/100  Signal level=-88 dBm\n  ESSID:\"My Wife\"\n")
    data.append("Cell 01 - Address: 08:96:D7:DB:1D:A9\n  Quality=70/70  Signal level=-31 dBm\n ESSID:\"Free My Wife\"\n          Cell 02 - Address: 90:5C:44:CE:C7:54\n                    Quality=54/70  Signal level=-56 dBm\n                    ESSID:\"KabelBox-7E5E\"\n          Cell 03 - Address: 86:5C:44:CE:C7:54\n                    Quality=55/70  Signal level=-55 dBm\n                    ESSID:\"Vodafone Hotspot\"\n          Cell 04 - Address: 08:96:D7:87:1D:A9\n                    Quality=63/70  Signal level=-39 dBm\n                    ESSID:\"Free My Wife\"\n          Cell 05 - Address: 00:9A:CD:E6:A7:E7\n                    ESSID:\"Telekom_FON\"\n                    Quality=93/100  Signal level=60/100\n")
    data.append("Cell 01 - Address: abc\n Quality=25/70  Signal level=-37dBm\n ESSID:\"Yeet\"\n   Cell 02 - Address: xyz\n Quality=10/70  Signal level=-15 dBm\n ESSID:\"Yeet\"\n   Cell 03 - Address: def\n Quality=99/100  Signal level=-91 dBm\n  ESSID:\"Free\"\n")
    for el in data:
        tmp = generate_input(el,locs[data.index(el)])
        td += tmp[0]
        head = tmp[1]
    return (td, head)

#-------------------------------------------------------------------------------------

def scan(interface):
    try:
        print(interface.upper()+":")
        ls = Popen(["sudo","iwlist", interface, "scan"],stdout=PIPE, universal_newlines=True)
        s = Popen(("grep", "Address\|ESSID\|Quality"), stdin=ls.stdout, stdout = PIPE)
        return s.communicate()[0]
    except:
        print("iwlist error")
        return None

#-------------------------------------------------------------------------------------

def get_config():
    mode, file, Y, = 0, "database.csv", "unspecified"
    ### If parameters are specified, take these instead of defaults
    if len(sys.argv) > 1:
        mode = int( sys.argv[1] )
    if len(sys.argv) > 2:
        file = sys.argv[2]
    if len(sys.argv) > 3:
        Y = sys.argv[3]
    ### Specify the intended interfaces
    ifaces = ["wlan%d" %i for i in range(mode)]
    ### Read the database
    db = from_csv(file)
    return ifaces, file, Y, db

#-------------------------------------------------------------------------------------

def get_netpic(ifaces,Y):
    ### Scan net picture for all specified interfaces
    if len(ifaces) == 0:
        return _testdata()
    elif len(ifaces) == 1:
        return generate_input(scan(ifaces[0]),Y)
    elif len(ifaces) == 2:
        np = []
        tmp = [generate_input(scan(el),Y) for el in ifaces]
        xy = list(zip(tmp[0],tmp[1]))
        tmp = [np.extend(el) for el in xy[0]]
        return np,xy[1][0]
    else:
        print("Wrong interface configuration")

#-------------------------------------------------------------------------------------

def generate_input(data,Y):
    table, cont, data = {},{"Loc":"","LocNr":""}, data.decode('utf8')
    ### Split into input values
    for cell in data.strip().split("   Cell"):
        try:
            cont["ESSID"] = re.search("ESSID.*", cell).group(0).replace("\"","").split(":")[1]
            addr = re.search("Address.*", cell).group(0).split(": ")
            cont["MAC"], cont["Nr"] = addr[1], dev( addr[1] )+100
            qs = re.search("(Quality.*) (Signal.*)[/|d]", cell)
            sig = qs.group(2).split("=")
            q = qs.group(1).split("=")[1].split("/")
            q = int( int(q[0])*100 / int(q[1]) )
            cont["Strength(dbm)"], cont["Quality(%)"] = -abs(int(sig[1])), q
            table[ cont["MAC"] ] = [cont[k] for k in sorted(cont)]
        except:
            print("weird string format")
    ### Sort and print table
    xy = [setY(table[k],Y) for k in sorted(table)]
    print(tabulate(xy, headers=sorted(cont), tablefmt='orgtbl' )+"\n")
    return [xy,sorted(cont)]

#-------------------------------------------------------------------------------------

def to_csv(table, head, csv_file):
    try:
        with open(csv_file, 'a') as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            if os.stat(csv_file).st_size == 0:
                writer.writerow(head)
            writer.writerows(table)
    except IOError:
        print("I/O error writing csv")

#-------------------------------------------------------------------------------------

def from_csv(csv_file):
    try:
        with open(csv_file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=";")
            res = [ ( [dev(r3)+100,int(r5),int(r6)], loc(r1)+100) for r0, r1, r2, r3, r4, r5, r6 in reader if r0 != "ESSID" ]
            if len(res) > 1:
                res = list(zip(*res))
            else:
                res = [[res[0][0]], [res[0][1]]]
            return res
    except IOError:
        print("I/O error reading csv")

#-------------------------------------------------------------------------------------

def learn(XY):
    TRAIN_INPUT,TRAIN_OUTPUT = XY
    predictor = LogisticRegression(solver='lbfgs', multi_class='ovr')
    predictor.fit(X=TRAIN_INPUT, y=TRAIN_OUTPUT)
    return predictor

#-------------------------------------------------------------------------------------

def guess(pred,test):
    g = [ LOCATIONS[xtest(pred,x)[0]-100] for x in test]
    guess = max(set(g), key=g.count)
    print("==> Result: %s" %guess)
    return guess
def xtest(pred,x):
    X_TEST = [ [DEVICES.index(x[0])+100] + x[1:] ]
    erg = pred.predict(X=X_TEST)
    print("Outcome: %s -> %s" %(format(erg),LOCATIONS[erg[0]-100]) )
    return erg

#------------------------------------------------------------------------------------

# ***********************************************************************************
if __name__ == "__main__":
    print( "WLANPOS main")
# ***********************************************************************************

#end of file
