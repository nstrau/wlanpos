#!/usr/bin/python

import re, csv, sys, os
from tabulate import tabulate
from subprocess import PIPE, Popen


def create_input(data):
    table = []
    for cell in data.split("   Cell"):
        cont={}
        erg = re.search("ESSID.*", cell)
        if erg != None:
            kv = erg.group(0).split(":")
            ssid = kv[1].split("\"")
            if ssid != None:
                cont[ kv[0] ] = ssid[1]
            else:
                cont[ kv[0] ] = kv[1]
        erg = re.search("Address.*", cell)
        if erg != None:
            kv = erg.group(0).split(": ")
            cont[ "Device" ] = kv[1]
        erg = re.search("(Quality.*) (Signal.*)[/|d]", cell)
        if erg != None:
            sig = erg.group(2).split("=")
            erg = erg.group(1).split("=")
            q = erg[1].split("/")
            q = int( int(q[0])*100 / int(q[1]) )
            cont["Level(dbm)"] = -abs(int(sig[1]))
            cont[erg[0]+"(%)" ] = q
        if cont:
            table.append(cont)
    return table

#-------------------------------------------------------------------------------------

def get_data(interface):
    try:
        ls = Popen(["sudo","iwlist", interface, "scan"],stdout=PIPE, universal_newlines=True)
        s = Popen(("grep", "Address\|ESSID\|Quality"), stdin=ls.stdout, stdout = PIPE)
        out = s.communicate()[0]
        return out
    except:
        return None

#-------------------------------------------------------------------------------------

def print_to_csv(table, mode, y, file):
    head, tmp, sortedlist = [], {}, []
    for line in table:
        line['Class'] = y
        head = sorted(line)
        dictlist=[]
        for k in head:
            dictlist.append( line[k] )
        tmp[ dictlist[1] ] = dictlist
    sort = sorted(tmp)
    for k in sort:
        sortedlist.append( tmp[k] )
    if not file:
        csv_file = "data.csv"
    else:
        csv_file = file
    try:
        with open(csv_file, 'a') as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            if os.stat(csv_file).st_size == 0:
                writer.writerow(head)
            writer.writerows(sortedlist)
    except IOError:
        print("I/O error")
    print(tabulate(sortedlist, headers=head, tablefmt='orgtbl' ))


#*************************************************************************************

if __name__ == '__main__':
    mode, y, file = int( sys.argv[1] ), sys.argv[2], ""
    if len(sys.argv) > 3:
        file = sys.argv[3]
    if mode == 2:
        data = get_data("wlan1")
        if data != None:
            table = create_input(data)
            print_to_csv(table, 1, y, file)
        print
        data = get_data("wlan0")
        if data != None:
            table = create_input(data)
            print_to_csv(table, 0, y, file)
    elif mode == 1:
        data = get_data("wlan0")
        if data != None:
            table = create_input(data)
            print_to_csv(table, 1, y, file)
    else:
        data = "          Cell 01 - Address: 08:96:D7:DB:1D:A9\n                    Quality=70/70  Signal level=-31 dBm\n                    ESSID:\"Free My Wife\"\n          Cell 02 - Address: 90:5C:44:CE:C7:54\n                    Quality=54/70  Signal level=-56 dBm\n                    ESSID:\"KabelBox-7E5E\"\n          Cell 03 - Address: 86:5C:44:CE:C7:54\n                    Quality=55/70  Signal level=-55 dBm\n                    ESSID:\"Vodafone Hotspot\"\n          Cell 04 - Address: 08:96:D7:87:1D:A9\n                    Quality=63/70  Signal level=-39 dBm\n                    ESSID:\"Free My Wife\"\n          Cell 05 - Address: 00:9A:CD:E6:A7:E7\n                    ESSID:\"Telekom_FON\"\n                    Quality=93/100  Signal level=60/100\n"
        table = create_input(data)
        print_to_csv(table, 1, y, file)


#end of file

