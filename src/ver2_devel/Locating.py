#!/usr/bin/python3

'''
Created on 09.05.2019

@author: nils
'''
from tabulate import tabulate
import wlanpos as wp

# ***********************************************************************************

if __name__ == '__main__':
    #get user input and existing data from csv
    ifaces, file, Y, db = wp.get_config()
    #get an iwlist net picture -> XY
    ###(important to take netpic before the learn, to take the new devices in account!)
    np = wp.get_netpic(ifaces, Y)
    #print the taken netpic for the user
    print(tabulate([ el[:-2] for el in np ], headers=wp.HEAD[:-2], tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    #train AI with data
    wp.learn(db, ifaces)
    #guess the location for this netpic
    XY = wp.guess(np, ifaces)
    #write back this netpic with guessed location to csv
    wp.to_csv(XY, file)

# ***********************************************************************************

#end of file
