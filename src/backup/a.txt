#!/usr/bin/python

import re
from subprocess import PIPE, Popen

interface = "wlan0"
table = []
ls = Popen(["iwlist", interface, "scan"],stdout=PIPE, universal_newlines=True)
s = Popen(("grep", "Address\|ESSID\|Quality"), stdin=ls.stdout, stdout = PIPE)
out = s.communicate()[0]

for cell in out.split("   Cell"):
    erg = re.search("ESSID.*", cell)
    if erg != None:
        kv = erg.group(0).split(":")
        cont[ kv[0] ] = kv[1]
    erg = re.search("Address.*", cell)
    if erg != None:
        kv = erg.group(0).split(": ")
        cont[ kv[0] ] = kv[1]
    erg = re.search("(Quality.*) (Signal.*) ", cell)
    if erg != None:
        sig = erg.group(2).split("=")
        erg = erg.group(1).split("=")
        q = erg[1].split("/")
        q = int(q[0]) / int(q[1])
        cont[sig[0]] = sig[1]
        cont[erg[0]] = q
    table.append(cont)



#eof

