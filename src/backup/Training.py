#!/usr/bin/python3

'''
Created on 22.04.2019

@author: nils
'''

import wlanpos as wp

# ***********************************************************************************
if __name__ == '__main__':
    #get config
    ifaces, file, Y, db  = wp.get_config()
    #get netpic/set Y -> XY
    np, head = wp.get_netpic(ifaces, Y)
    #to csv
    wp.to_csv(np, head, file)
# ***********************************************************************************

#end of file