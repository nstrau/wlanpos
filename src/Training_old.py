#!/usr/bin/python3

'''
Created on 22.04.2019

@author: nils
'''

import oldpos as op

# ***********************************************************************************
if __name__ == '__main__':
    #get config
    ifaces, file, Y, db  = op.get_config()
    #get netpic/set Y -> XY
    np, head = op.get_netpic(ifaces, Y)
    #to csv
    op.to_csv(np, head, file)
# ***********************************************************************************

#end of file