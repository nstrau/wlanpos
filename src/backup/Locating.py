#!/usr/bin/python3

'''
Created on 22.04.2019

@author: nils
'''

import wlanpos as wp

# ***********************************************************************************
if __name__ == '__main__':
    #get config
    ifaces, file, Y, db = wp.get_config()
    #from csv
    db = wp.from_csv(file)
    #learn database
    pred = wp.learn(db)

    #get netpic -> XY
    np, head = wp.get_netpic(ifaces, '.'*3 )
    #get X
#    X = wp.getX(np)
#    X = [['A8:BD:27:44:A4:43',88,-67]]
    X = [['20:A6:CD:3F:01:A3',47,-63]]
    #guess X
    Y = wp.guess(pred, X)
    #set guessed Y
    table = [wp.setY(el, Y) for el in np]
    #to csv
#    wp.to_csv(table, head, file)
# ***********************************************************************************

#end of file
