#!/usr/bin/python3

'''
Created on 09.05.2019

@author: nils
'''

import sys
from tabulate import tabulate
import wlanpos as wp

def get_config():
    mode, file, times = 0,'',1
    ### If parameters are specified, take these instead of defaults
    if len(sys.argv) > 1:
        mode = int( sys.argv[1] )
    if len(sys.argv) > 2:
        file = sys.argv[2]
    if len(sys.argv) > 3:
        times = int(sys.argv[3])
    ### Specify the intended interfaces
    ifaces = ["wlan%d" %i for i in range(mode)]
    if not ifaces:
        ifaces = ["none"]
    return ifaces, file, times


def locate(ifaces, db):
    #get an iwlist net picture -> XY
    np = wp.get_netpic(ifaces)
    #print the taken netpic for the user
    print(tabulate([ el[:-2] for el in np ], headers=wp.HEAD[:-2], tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    #train AI with data
    wp.learn(db, ifaces)
    #guess the location for this netpic and add it to the netpic
    XY = wp.guess(np, ifaces)
    return XY

# ***********************************************************************************

if __name__ == '__main__':
    #get user input and existing data from csv
    ifaces, file, times = get_config()
    #get all already recorded data
    db = wp.from_csv(file)
    #get an iwlist net picture -> XY
    ###(important to take netpic before the learn, to take the new devices in account!)
    np = wp.get_netpic(ifaces)
    #print the taken netpic for the user
    print(tabulate([ el[:-2] for el in np ], headers=wp.HEAD[:-2], tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    #train AI with data
    wp.learn(db, ifaces)
    #guess the location for this netpic and add it to the netpic
    XY = wp.guess(np, ifaces)
    
    #guess more often if intended
    for i in range(1,times):
        np += locate(ifaces, db)
    
    #write back this netpic with guessed location to csv
    #  wp.to_csv(XY, file)

# ***********************************************************************************

#end of file
