#!/usr/bin/python3

'''
Created on 09.05.2019

@author: nils
'''
import time, sys
from tabulate import tabulate
import wlanpos as wp

def get_config():
    mode, file, times = 0,'',1
    ### If parameters are specified, take these instead of defaults
    if len(sys.argv) > 1:
        mode = int( sys.argv[1] )
    if len(sys.argv) > 2:
        file = sys.argv[2]
    if len(sys.argv) > 3:
        Y = sys.argv[3]
    else:
        Y = 'unspecified'
    if len(sys.argv) > 4:
        times = int(sys.argv[4])
    ### Specify the intended interfaces
    ifaces = ["wlan%d" %i for i in range(mode)]
    if not ifaces:
        ifaces = ["none"]
    return ifaces, file, Y, times 


def train(ifaces, Y):
    #get an iwlist net picture -> XY
    np = wp.get_netpic(ifaces)
    #fill in the name of the location for this netpic
    np = [wp._setY(el,Y) for el in np]
    #print the taken netpic for the user
    print(tabulate(np, headers=wp.HEAD, tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    return np

# ***********************************************************************************
if __name__ == '__main__':
    #get config
    ifaces, file, Y, times = get_config()
    #get all mentioned Locations and how many times they occur
    db = wp.from_csv(file)
    #add the new location
    wp.locCntUp(Y)
    #get training input at least a single time
    table = train(ifaces, Y)
    #get training input as often as specified in variable times
    for i in range(1,times):
        #short delay between the single tries, so that wlan resource is not busy
       ###  time.sleep(0.5)
        #count up the time this location is used
        wp.locCntUp(Y)
        #get another netpic
        table += train(ifaces, Y)
    #write back all the netpics to csv
    wp.to_csv(table, file)
# ***********************************************************************************

#end of file
