'''
Created on 09.05.2019

@author: nils
'''

import re, csv, os, sys
from tabulate import tabulate
from subprocess import PIPE, Popen
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import OneHotEncoder


#-------------------------------------------------------------------------------------

PRED = LogisticRegression(solver='liblinear', multi_class='auto')
ENC = OneHotEncoder(handle_unknown='ignore',sparse=False)
DEVICES, LOCATIONS = {}, {}
HEAD = ["AP","Device","Interface","Level","Location","Times"]
APs = ["Free My Wife", "public","eduroam","campus","@BayernWLAN"]

IFACES = []
#-------------------------------------------------------------------------------------

def _loc(Y):
    if Y in LOCATIONS:
        LOCATIONS[Y]+=1
    else:
        LOCATIONS[Y]=1
    return Y

#-------------------------------------------------------------------------------------

def _setY(el,Y):
    el[-2], el[-1] = Y, -(-LOCATIONS[Y]//len[IFACES])
    return el

#-------------------------------------------------------------------------------------

def _scaleDev(el,s):
    el[:len(DEVICES)] = [ i*s for i in el[:len(DEVICES)]]
    return el

#-------------------------------------------------------------------------------------

def _convDbm(level):
    if level<-92:
        return 0.01
    elif level>-21 and level<0:
        return 1.0
    elif level<0: 
        return round(-0.0154*level*level - 0.3794*level + 98.182)/100
    else:
        return level/100

#-------------------------------------------------------------------------------------

def _convLine(line):    
    _loc(line[-2])
    DEVICES[line[1]]=0
    line[3] = float(line[3])
    return line

#-------------------------------------------------------------------------------------

def _gen(data, interface, Y):
    table, cont = {}, { HEAD[2]: interface, HEAD[4]:'', HEAD[5]: 0, }
    for cell in data.strip().split("   Cell"):
        essid = re.search("ESSID.*", cell)
        if essid:
            essid = essid.group(0).replace("\"","").split(":")[1]
            if essid in APs:
                cont["{:<20}".format(HEAD[0])] = essid
                cont[HEAD[1]] = re.search("Address.*", cell).group(0).split(": ")[1]
                DEVICES[cont[HEAD[1]]] = 0
                cont[HEAD[3]] = _convDbm( float( re.search("Signal level=(-\d*|\d*)", cell).group(1)))
                table[ cont[HEAD[1]] ] = [cont[k] for k in sorted(cont)]
    xy = [_setY(table[k],Y) for k in sorted(table)]
    print(tabulate(xy, headers=HEAD, tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    return xy

#-------------------------------------------------------------------------------------

def _transform(XY):
    FIT = [ [k, IFACES[0]] for k in DEVICES.keys() ]
    if len(IFACES)>1 and len(FIT)>1:
        FIT[1][1] = IFACES[1]
    ENC.fit(FIT)
    X = [ ENC.transform([ el[1:3] ]).tolist()[0] + [el[3]] for el in XY]
    X = [ _scaleDev(el,100) for el in X]
    Y = [ el[-2] for el in XY]
    return (X,Y)

#-------------------------------------------------------------------------------------

def _scan(interface):
    try:
        print(interface.upper()+":")
        ls = Popen(["sudo","iwlist", interface, "scan"],stdout=PIPE, universal_newlines=True)
        s = Popen(("grep", "Address\|ESSID\|Quality"), stdin=ls.stdout, stdout = PIPE)
        return s.communicate()[0].decode('utf8')
    except:
        print("iwlist error")
        return None

#-------------------------------------------------------------------------------------
def _test(Y):
    data = ["   Cell 01 - Address: 6E:C7:EC:FF:D0:ED\n Signal level=24/100\n ESSID:\"public\"\n   Cell 02 - Address: efg\n Quality=63/100  Signal level=63/100\n  ESSID:\"Free My Wife\"\n" ]
    XY = _gen(data[0],"none",Y)
    return XY
    
#-------------------------------------------------------------------------------------

def get_config():
    mode, file, Y, = 0, "database.csv", _loc("unspecified")
    ### If parameters are specified, take these instead of defaults
    if len(sys.argv) > 1:
        mode = int( sys.argv[1] )
    if len(sys.argv) > 2:
        file = sys.argv[2]
    if len(sys.argv) > 3:
        Y = _loc(sys.argv[3])
    ### Specify the intended interfaces
    IFACES = ["wlan%d" %i for i in range(mode)]
    if not IFACES:
        IFACES = ["none"]
    db = from_csv(file)
    return IFACES, file, Y, db

#-------------------------------------------------------------------------------------

def get_netpic(Y):
    XY = []
    if IFACES != ["none"]:
        for el in IFACES:
            XY += _gen( _scan(el), el, Y)
    else:
        XY = _test(Y)
    return XY

#-------------------------------------------------------------------------------------

def to_csv(table, csv_file):
    try:
        with open(csv_file, 'a') as csvfile:
            writer = csv.writer(csvfile, delimiter=";")
            if os.stat(csv_file).st_size == 0:
                writer.writerow(HEAD)
            writer.writerows(table)
    except IOError:
        print("I/O error writing csv")

#-------------------------------------------------------------------------------------

def from_csv(csv_file):
    try:
        with open(csv_file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=";")
            res = [ _convLine(line) for line in reader if line[0] != HEAD[0]]
            print(tabulate(res, headers=HEAD, tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
            return res
    except IOError:
        print("I/O error reading csv")

#-------------------------------------------------------------------------------------

def learn(db):
    TI,TO = _transform(db)
    PRED.fit(X=TI, y=TO)

#-------------------------------------------------------------------------------------

def guess(netpic):
    TEST = _transform(netpic)[0]
    TEST = list(PRED.predict(TEST))
    guess = max(TEST, key=TEST.count)
    print("%s ==> Result: %s\n" %(TEST,guess) )
    XY = [ _setY(el, guess) for el in netpic]
    return XY

#------------------------------------------------------------------------------------

# ***********************************************************************************
if __name__ == "__main__":
    print( "WLANPOS main")
#     ifaces, file, Y, db = ["none"],'Fin.csv',_loc('HIERabernichtDa'),from_csv('Fin.csv')
#     #train AI with data
#     print(db)
#     
#     #get an iwlist net picture -> XY
#     np = get_netpic(ifaces, Y)
# #     print(np)
#     learn(db, ifaces)
# #     TEST = _transform(np, ifaces)[0]
# #     print(TEST)
#     print(DEVICES)
#     print(LOCATIONS)
#     #guess the location for this netpic
#     XY = guess(np, ifaces)
# #     #write back this netpic with guessed location to csv
# #     print(tabulate(np, headers=HEAD, tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
# #     xy = [ _setY(el,Y) for el in XY]
# #     print(tabulate(xy, headers=HEAD, tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
# ***********************************************************************************

#end of file