#!/usr/bin/python3

'''
Created on 22.04.2019

@author: nils
'''

import sys
import oldpos as op
from tabulate import tabulate
import wlanpos as wp

# ***********************************************************************************
if __name__ == '__main__':
    #get config (special for this Script)
    mode = int( sys.argv[1] )
    file = sys.argv[2]
    file_old = sys.argv[3]
    ifaces = ["wlan%d" %i for i in range(mode)]
    db = wp.from_csv(file)
    
    
    ### VERSION 1
    print("--- Version 1 ---")
    #from csv
    db_old = op.from_csv(file_old)
    #learn database
    pred_old = op.learn(db_old)
    #get netpic -> XY
    np_old, head_old = op.get_netpic(ifaces, '.'*3 )
    #get X
    X = op.getX(np_old)
    #guess X
    Y_old = op.guess(pred_old, X)
    #set guessed Y
    table = [op.setY(el, Y_old) for el in np_old]
    #to csv
    op.to_csv(table, head_old, file_old)
    
    ### VERSION 2
    print("\n--- Version 2 ---")
    if not ifaces:
        ifaces = ["none"]
    #get an iwlist net picture -> XY
    #(important to take netpic before the learn, to take the new devices in account!)
    np = wp.get_netpic(ifaces)
    #print the taken netpic for the user
    print(tabulate([ el[:-2] for el in np ], headers=wp.HEAD[:-2], tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    #train AI with data
    wp.learn(db, ifaces)
    #guess the location for this netpic
    XY = wp.guess(np, ifaces)
    #write back this netpic with guessed location to csv
    wp.to_csv(XY, file)
# ***********************************************************************************

#end of file
