#!/usr/bin/python3

'''
Created on 09.05.2019

@author: nils
'''

from tabulate import tabulate
import wlanpos as wp


# ***********************************************************************************
if __name__ == '__main__':
    #get config
    ifaces, file, Y, db  = wp.get_config()
    #get an iwlist net picture -> XY
    np = wp.get_netpic(ifaces, Y)
    #fill in the name of the location for this netpic
    np = [wp._setY(el,Y) for el in np]
    #print the taken netpic for the user
    print(tabulate(np, headers=wp.HEAD, tablefmt='orgtbl', numalign="left", floatfmt=".2f" )+"\n")
    #write back the netpic to csv
    wp.to_csv(np, file)
# ***********************************************************************************

#end of file